#include "keyboard.h"

#include <unistd.h>
#include <termios.h>
#include <poll.h>

#include <mutex>

namespace pocketinit
{

static
struct termios gTerminalSettings;

static
void init_keyboard()
{
  static std::once_flag once;
  std::call_once(once, []() {
    tcgetattr(STDIN_FILENO, &gTerminalSettings);
  });
}

void Keyboard::setCanonicalMode(bool on)
{
  init_keyboard();

  if(on) {
    gTerminalSettings.c_lflag |= ICANON;
  } else {
    gTerminalSettings.c_lflag &= ~(ICANON);
  }

  tcsetattr(STDIN_FILENO, TCSANOW, &gTerminalSettings);

  if(on) {
    setlinebuf(stdin);
  } else {
    setbuf(stdin, NULL);
  }
}

bool Keyboard::isKeyPressed(unsigned timeout_ms)
{
  init_keyboard();

  struct pollfd pls[ 1 ];
  pls[ 0 ].fd     = STDIN_FILENO;
  pls[ 0 ].events = POLLIN | POLLPRI;
  return poll(pls, 1, timeout_ms) > 0;
}


} // namespace pocketinit