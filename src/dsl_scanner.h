#pragma once

#include "dsl_bison.hh"
#include "location.hh"

namespace pocketinit
{

class DSL_Scanner : public dslFlexLexer
{
public:
  DSL_Scanner(std::istream* in) : dslFlexLexer(in)
  {
    loc = new pocketinit::DSL_Parser::location_type();
  }

  ~DSL_Scanner()
  {
    delete loc;
  }

  using FlexLexer::yylex;

  /** YY_DECL defined int dsl.l
   * method body created by flex in dsl.yy.cc
   */
  virtual
  int yylex(pocketinit::DSL_Parser::semantic_type* const lval,
            pocketinit::DSL_Parser::location_type* location);

private:
  pocketinit::DSL_Parser::semantic_type* yylval = nullptr;
  pocketinit::DSL_Parser::location_type* loc = nullptr;
};

} // namespace pocketinit