#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>

#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include "pocketinit.h"
#include "keyboard.h"

#include <list>

using namespace pocketinit;

namespace pocketinit
{

std::list<Process> gProcesses;

} // namespace pocketinit

/* All known arches use small ints for signals */
smallint got_signal;

DSLDriver driver;

static void pause_and_low_level_reboot(unsigned magic) NORETURN;
static void pause_and_low_level_reboot(unsigned magic)
{
	pid_t pid;

	/* Allow time for last message to reach serial console, etc */
	sleep(1);

	/* We have to fork here, since the kernel calls do_exit(EXIT_SUCCESS)
	 * in linux/kernel/sys.c, which can cause the machine to panic when
	 * the init process exits... */
	pid = vfork();
	if (pid == 0) { /* child */
		reboot(magic);
		_exit(EXIT_SUCCESS);
	}
	/* Used to have "while (1) sleep(1)" here.
	 * However, in containers reboot() call is ignored, and with that loop
	 * we would eternally sleep here - not what we want.
	 */
	waitpid(pid, NULL, 0);
	sleep(1); /* paranoia */
	_exit(EXIT_SUCCESS);
}

static
void run_shutdown_and_kill_processes(void)
{
	driver.fire_event("shutdown");

	message(L_CONSOLE | L_LOG, "The system is going down NOW!");

	/* Send signals to every process _except_ pid 1 */
	kill(-1, SIGTERM);
	message(L_CONSOLE, "Sent SIG%s to all processes", "TERM");
	sync();
	sleep(1);

	kill(-1, SIGKILL);
	message(L_CONSOLE, "Sent SIG%s to all processes", "KILL");
	sync();
	/*sleep(1); - callers take care about making a pause */
}

static void halt_reboot_pwoff(int sig) NORETURN;
static void halt_reboot_pwoff(int sig)
{
	const char *m;
	unsigned rb;

	/* We may call run() and it unmasks signals,
	 * including the one masked inside this signal handler.
	 * Testcase which would start multiple reboot scripts:
	 *  while true; do reboot; done
	 * Preventing it:
	 */
	reset_sighandlers_and_unblock_sigs();

	run_shutdown_and_kill_processes();

	m = "halt";
	rb = RB_HALT_SYSTEM;
	if (sig == SIGTERM) {
		m = "reboot";
		rb = RB_AUTOBOOT;
	} else if (sig == SIGUSR2) {
		m = "poweroff";
		rb = RB_POWER_OFF;
	}
	message(L_CONSOLE, "Requesting system %s", m);
	pause_and_low_level_reboot(rb);
	/* not reached */
}



void record_signo(int signo)
{
  printf("got signal 0x%x\n", signo);
	got_signal = signo;
}

static
void signals_recursive_norestart(int sigs, void (*f)(int))
{
	int sig_no = 0;
	int bit = 1;
	struct sigaction sa;

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = f;
	/*sa.sa_flags = 0;*/
	/*sigemptyset(&sa.sa_mask); - hope memset did it*/

	while (sigs) {
		if (sigs & bit) {
			sigs -= bit;
			sigaction_set(sig_no, &sa);
		}
		sig_no++;
		bit <<= 1;
	}
}

static
void setupSignalHandlers()
{
  signals_recursive_norestart(0
        + (1 << SIGINT)  /* Ctrl-Alt-Del */
        + (1 << SIGQUIT) /* re-exec another init */
        + (1 << SIGPWR)  /* halt */
        + (1 << SIGUSR1) /* halt */
        + (1 << SIGTERM) /* reboot */
        + (1 << SIGUSR2) /* poweroff */
        , record_signo);
}

static
void checkDelayedSignals()
{
  int sigs_seen = 0;

	while (1) {
		smallint sig = got_signal;

		if (!sig)
			return sigs_seen;
		bb_got_signal = 0;
		sigs_seen = 1;

		if (sig == SIGQUIT) {
			exec_restart_action();
			/* returns only if no restart action defined */
		}
		if ((1 << sig) & (0
		    + (1 << SIGPWR)
		    + (1 << SIGUSR1)
		    + (1 << SIGUSR2)
		    + (1 << SIGTERM)
		)) {
			halt_reboot_pwoff(sig);
		}
	}
  
}

static void console_init()
{
  int vtno;
  char* s;

  s = getenv("CONSOLE");
  if(!s) {
    s = getenv("console");
  }

  if(s) {
    int fd = open(s, O_RDWR | O_NONBLOCK | O_NOCTTY);
    if(fd >= 0) {
      dup2(fd, STDIN_FILENO);
      dup2(fd, STDOUT_FILENO);
      dup2(fd, STDERR_FILENO);
      close(fd);
    }
    message(L_CONSOLE, "console='%s'", s);
  }


}

static
void manage_exit_process(int pid)
{
  std::list<Process>::iterator it;

  for(it=gProcesses.begin(); it!=gProcesses.end(); it++) {
    Process& p = *it;
    if(p.pid == pid) {
      if(p.flags & FLAG_RELAUNCH) {
        message(L_CONSOLE | L_LOG, "Restarting %s...", p.args[0].c_str());
        p.pid = run(p.args);
      } else {
        gProcesses.erase(it);
      }
      break;
    }
  }
}

int main(int argc, char* argv[])
{

  if(getpid() != 1) {
    message(L_CONSOLE, "must be run as PID 1");
    exit(-1);
  }

  console_init();

  chdir("/");
  setsid();

  /* Make sure environs is set to something sane */
  putenv((char*) "HOME=/");
  putenv((char*) "PATH=/sbin:/usr/sbin:/bin:/usr/bin");
  putenv((char*) "SHELL=/bin/sh");
  putenv((char*) "USER=root");

  message(L_CONSOLE | L_LOG, "pocketinit started");

  driver.parseFile("/etc/pocketinit.conf");

  driver.fire_event("earlyinit");

  message(L_CONSOLE, "Press any key to interrupt boot process...");

  Keyboard::setCanonicalMode(false);
  bool interrupt_boot = Keyboard::isKeyPressed(3000);
  Keyboard::setCanonicalMode(true);

  if(interrupt_boot) {
    //interrupt normal boot process and drop to terminal
    message(L_CONSOLE, "Boot process interrupted");
    Process console;
    console.args = {"sh"};
    console.flags = FLAG_RELAUNCH;
    console.pid = run(console.args);
    gProcesses.push_back(console);

  } else {
    message(L_CONSOLE, "Continuing boot process");
    driver.fire_event("update");
    driver.fire_event("boot");
  }

  setupSignalHandlers();

  while(1) {

    /* Wait for any child process(es) to exit */
    while(1) {
      pid_t wpid;
      struct init_action* a;

      wpid = waitpid(-1, NULL, WNOHANG);
      if(wpid <= 0) {
        break;
      }

      message(L_CONSOLE | L_LOG, "pid %u exited", (unsigned) wpid);
      manage_exit_process((unsigned)wpid);
    }

    checkDelayedSignals();

    sleep(1);
  }
}

void yyerror(const char* s)
{
  printf("EEK, parse error %s\n", s);
  exit(-1);
}