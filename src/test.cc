#include <gtest/gtest.h>

#include <string>


#include "scanners.h"
#include "udev_scanner.h"
#include "dsl_scanner.h"


#include "udev_file.h"
#include "DSL.h"

using namespace pocketinit;

extern "C" {
#include <gc.h>
}


TEST(udev_parser, test1)
{

  const std::string raw_input(R"(
    
    INPUT=kewl
    
  )");

  std::istringstream istream(raw_input);

  UDevFile driver;
  driver.parse(istream);


}

TEST(dsl_parser, test1)
{
  const std::string raw_input(R"(

    on earlyinit {
      echo helloworld;
      mount -t sysfs /sys /sys;
    }

    service bluetooth /sbin/bluetoothd {
      
    }


  )");

  std::istringstream istream(raw_input);

  DSLDriver driver;
  driver.parse(istream);

  driver.fire_event("earlyinit");
}