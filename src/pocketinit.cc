
#include "pocketinit.h"

#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

namespace pocketinit
{


/*
 * Write all of the supplied buffer out to a file.
 * This does multiple writes as necessary.
 * Returns the amount written, or -1 on an error.
 */
static ssize_t full_write(int fd, const void* buf, size_t len)
{
  ssize_t cc;
  ssize_t total;

  total = 0;

  while(len) {
    cc = write(fd, buf, len);

    if(cc < 0) {
      if(total) {
        /* we already wrote some! */
        /* user can do another write to know the error code */
        return total;
      }
      return cc;  /* write() returns -1 on failure. */
    }

    total += cc;
    buf = ((const char*)buf) + cc;
    len -= cc;
  }

  return total;
}

void message(int where, const char* fmt, ...)
{
  va_list arguments;
  unsigned l;
  char msg[128];

  msg[0] = '\r';
  va_start(arguments, fmt);
  l = 1 + vsnprintf(msg + 1, sizeof(msg) - 2, fmt, arguments);
  if(l > sizeof(msg) - 2) {
    l = sizeof(msg) - 2;
  }
  va_end(arguments);

  msg[l] = '\0';
  msg[l++] = '\n';
  msg[l] = '\0';

  if(where & L_CONSOLE) {
    full_write(STDERR_FILENO, msg, l);
  }
}

int run(const std::vector<std::string>& cmd_line)
{
  int pid;
  int err;

  pid = fork();
  if(pid == 0) {
    //child
    setsid();
    exec(cmd_line);
  } else {
    return pid;
  }

  return pid;
}

void exec(const std::vector<std::string>& cmd_line)
{
  int err;
  const char** args = reinterpret_cast<const char**>(alloca(sizeof(char*) * cmd_line.size()+1));
  for(int i=0; i<cmd_line.size(); i++) {
    args[i] = cmd_line[i].c_str();
  }
  args[cmd_line.size()] = nullptr;

  err = execvp(args[0], (char* const*)args);
  message(L_LOG | L_CONSOLE, "can't run '%s': %s", args[0], strerror(errno));
}

} // namespace pocketinit