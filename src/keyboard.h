#pragma once

namespace pocketinit
{

struct Keyboard {

  /**
   * when canonical mode is enabled, input is made available line by line
   */
  static
  void setCanonicalMode(bool);

  static
  bool isKeyPressed(unsigned timeout_ms = 0);

};

} // namespace