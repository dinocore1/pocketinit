#pragma once

#include "udev_bison.hh"
#include "location.hh"

namespace pocketinit
{

class UDev_Scanner : public udevFlexLexer
{
public:
  UDev_Scanner(std::istream* in) : udevFlexLexer(in)
  {
    loc = new pocketinit::UDev_Parser::location_type();
  }

  ~UDev_Scanner()
  {
    delete loc;
  }

  using FlexLexer::yylex;

  /** YY_DECL defined int udev.l
   * method body created by flex in udev.yy.cc
   */
  virtual
  int yylex(pocketinit::UDev_Parser::semantic_type* const lval,
            pocketinit::UDev_Parser::location_type* location);

private:
  pocketinit::UDev_Parser::semantic_type* yylval = nullptr;
  pocketinit::UDev_Parser::location_type* loc = nullptr;
};

} // pocketinit