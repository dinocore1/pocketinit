%option prefix="dsl"
%option debug
%option c++
%option nodefault
%option yyclass="pocketinit::DSL_Scanner"
%option noyywrap

%{

  #include <string>
  #include "DSLDriver.h"
  #include "dsl_scanner.h"

  #undef YY_DECL
  #define YY_DECL int pocketinit::DSL_Scanner::yylex(pocketinit::DSL_Parser::semantic_type* const lval, pocketinit::DSL_Parser::location_type* location)

  using token = pocketinit::DSL_Parser::token;

  /* define yyterminate as this insteam of NULL */
  #define yyterminate() return (token::TEND)

  /* update location on matching */
  #define YY_USER_ACTION loc->step(); loc->columns(yyleng);

%}

%%

%{
  /** code executed at the beginning of yylex **/
  yylval = lval;
%}

";"                         { return token::TSEMI; }
"="                         { return token::TEQUALS; }
"{"                         { return token::TLBRACE; }
"}"                         { return token::TRBRACE; }
"service"                   { return token::TSERVICE; }
"on"                        { return token::TON; }
[a-zA-Z0-9_/\-.]*           { yylval->build<std::string>(yytext); return token::TID; }
\n                          { loc->lines(); }
[ \t]                       {}

%%