#pragma once


#include "scanners.h"
#include "udev_scanner.h"
#include "dsl_scanner.h"


#include "udev_file.h"
#include "DSLDriver.h"

#include <string>
#include <vector>

namespace pocketinit
{

#define FLAG_RELAUNCH (1 << 0)

struct Process {
  int pid;
  int flags;
  std::vector<std::string> args;
};

enum {
  L_LOG = 0x1,
  L_CONSOLE = 0x2,
};

void message(int where, const char* fmt, ...)
__attribute__((format(printf, 2, 3)));

int run(const std::vector<std::string>& cmdline);

void exec(const std::vector<std::string>& cmdline);

} // namespace pocketinit