#include "pocketinit.h"
#include "scanners.h"
#include "dsl_scanner.h"

#include "DSLDriver.h"

#include <fstream>

#include <sys/types.h>
#include <sys/wait.h>

namespace pocketinit
{

DSLDriver::~DSLDriver()
{}

void DSLDriver::parse(std::istream& iss)
{
  scanner = std::make_unique<DSL_Scanner>(&iss);
  parser = std::make_unique<DSL_Parser>(*scanner, *this);
  //parser->set_debug_stream(std::cout);
  //parser->set_debug_level(5);
  const int accept(0);
  if(parser->parse() != accept) {
    message(L_CONSOLE | L_LOG, "Parse failed");
    //std::cerr << "Parse failed" << std::endl;
  }
}

void DSLDriver::parseFile(const std::string& filepath)
{
  message(L_CONSOLE | L_LOG, "read file: %s", filepath.c_str());
  std::ifstream myfile(filepath);
  parse(myfile);
}

void DSLDriver::add_event(const str& name, const std::vector<str_list>& cmds)
{
  event_def[name] = cmds;
}

void DSLDriver::add_service(const str& name, const str_list& cmd, const str_map& properties)
{
  services.push_back({
    .name = name,
    .cmd = cmd,
    .properties = properties,
  });
}

void DSLDriver::fire_event(const str& name)
{
  int status;

  message(L_CONSOLE, "fire event: %s", name.c_str());
  std::vector<str_list>& def = event_def[name];
  for(const str_list& cmd_line : def) {

    if(cmd_line[0].compare("exec") == 0) {
      str_list cmd_line1(cmd_line.size() - 1);
      std::copy(cmd_line.begin()+1, cmd_line.end(), cmd_line1.begin());
      exec(cmd_line1);
    } else {
      int pid = run(cmd_line);
      waitpid(pid, &status, 0);
      if(!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
        message(L_CONSOLE, "cmd %s exited with status: %d", cmd_line[0].c_str(), WEXITSTATUS(status));
      }
    }
  }

}

} // namespace pocketinit