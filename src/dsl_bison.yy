%skeleton "lalr1.cc"
%require "3.2"
%language "c++"
%debug
%defines
%define api.namespace {pocketinit}
%define api.parser.class {DSL_Parser}

%parse-param { DSL_Scanner &scanner }
%parse-param { DSLDriver &driver }

%code requires{

  #include <string>
  #include <vector>
  #include <map>

  namespace pocketinit {
    class DSLDriver;
    class DSL_Scanner;
  }
}

%code{
  #include <iostream>
  #include <cstdlib>
  #include <fstream>
  

  #include "scanners.h"
  #include "dsl_scanner.h"

  #include "DSLDriver.h"
  
  #undef yylex
  #define yylex scanner.yylex
}

%define api.value.type variant
%define parse.assert
%locations

%token TEND 0 "end of file"
%token TSERVICE TON TLBRACE TRBRACE TEQUALS TSEMI
%token <std::string> TID
%type <std::vector<std::vector<std::string>>> cmdlist
%type <std::vector<std::string>> cmd
%type <std::map<std::string, std::string>> dictionary
%type <std::pair<std::string, std::string>> keyvalue

%%

dsl_file
  : items TEND
  ;

items
  : items service
  | items event
  |
  ;

service
  : TSERVICE TID cmd TLBRACE dictionary TRBRACE
   { driver.add_service($2, $3, $5); }
  ;

event
  : TON TID TLBRACE cmdlist TRBRACE
    { driver.add_event($2, $4); }
  ;

cmdlist
  : cmdlist cmd TSEMI { $1.push_back($2); $$ = $1; }
  | { $$ = std::vector<std::vector<std::string>>(); }
  ;

cmd
  : cmd TID { $1.push_back($2); $$ = $1; }
  | TID { $$ = std::vector<std::string>(); $$.push_back($1); }
  ;

dictionary
  : dictionary keyvalue { $$.insert($2); $$ = $1; }
  | { $$ = std::map<std::string, std::string>(); }
  ;

keyvalue
  : TID TEQUALS TID { $$ = std::make_pair($1, $3); }
  ;

%%

void
pocketinit::DSL_Parser::error( const location_type &l, const std::string &err_message )
{
  std::cerr << "Error: " << err_message << " at " << l << std::endl;
}