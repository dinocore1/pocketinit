%option prefix="udev"
%option debug
%option c++
%option nodefault
%option yyclass="pocketinit::UDev_Scanner"
%option noyywrap

%{
  #include <string>

  #include "udev_file.h"
  #include "udev_scanner.h"

  #undef YY_DECL
  #define YY_DECL int pocketinit::UDev_Scanner::yylex(pocketinit::UDev_Parser::semantic_type* const lval, pocketinit::UDev_Parser::location_type* location)

  using token = pocketinit::UDev_Parser::token;

  /* define yyterminate as this insteam of NULL */
  #define yyterminate() return (token::TEND)

  /* update location on matching */
  #define YY_USER_ACTION loc->step(); loc->columns(yyleng);


%}

%%

%{
  /** code executed at the beginning of yylex **/
  yylval = lval;
%}

"="                         { return token::TEQUALS; }
[a-zA-Z0-9_]*               { yylval->build<std::string>(yytext); return token::TID; }
\n                          { loc->lines(); return token::TNEWLINE; }
[ \t]                       {}
%%