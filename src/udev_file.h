#pragma once

#include <string>
#include <cstddef>
#include <istream>
#include <memory>

#include "udev_bison.hh"

namespace pocketinit
{

class UDevFile
{
public:
  UDevFile() = default;
  virtual ~UDevFile();

  void parse(std::istream& iss);

  void add_keyvalue(const std::string& key, const std::string& value);

private:

  std::unique_ptr<UDev_Parser> parser;
  std::unique_ptr<UDev_Scanner> scanner;


};

} // namespace pocketinit