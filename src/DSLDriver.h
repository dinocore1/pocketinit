#pragma once

#include <string>
#include <cstddef>
#include <istream>
#include <memory>
#include <vector>
#include <map>

#include "dsl_bison.hh"

namespace pocketinit
{

class DSLDriver
{
public:

  typedef std::string str;
  typedef std::vector<std::string> str_list;
  typedef std::map<std::string, std::string> str_map;

  struct service {
    str name;
    str_list cmd;
    str_map properties;
  };

  std::vector<service> services;
  std::map<str, std::vector<str_list>> event_def;


  DSLDriver() = default;
  virtual ~DSLDriver();

  void parse(std::istream& iss);
  void parseFile(const str& filepath);
  void add_event(const str& name, const std::vector<str_list>& cmds);
  void add_service(const str& name, const str_list& cmd, const str_map& properties);
  void fire_event(const str& name);

private:
  std::unique_ptr<DSL_Parser> parser;
  std::unique_ptr<DSL_Scanner> scanner;
};

} // namespace pocketinit